USE c9;

CREATE TABLE user (
    email VARCHAR(180) NOT NULL PRIMARY KEY,
    password VARCHAR(80) NOT NULL,
    hasAccess INTEGER(1)
);

CREATE TABLE post (
    id INT NOT NULL AUTO_INCREMENT PRIMARY KEY,
    email VARCHAR(180) NOT NULL,
    title VARCHAR(256) NOT NULL,
    posting VARCHAR(1024) NOT NULL,
    date_posted DATE,
    intent VARCHAR(30),
    deleted BOOLEAN NOT NULL,
    FOREIGN KEY (email) REFERENCES user(email)
);
