<?php
  require_once('DRY.php');
  $intent_slugged = $_GET['intent'];
  $unslugged = unslug($intent_slugged);
  head('WCP: '. ucfirst($unslugged));
    ?>
    <h3>
      <?php
        print $unslugged;
      ?>
    </h3>
    <?php
      listings($unslugged);
      footer();
    ?>
  </div>
</div>
</html>