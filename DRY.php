<?php
ini_set('display_errors', 'On');
require_once('Dao.php');
require_once('utilities.php');

function head($title) {
  $new_head = '
  <!DOCTYPE html>
  <html>
  <head>
    <title>' . $title . '</title>
    <meta charset=utf-8 />
    <link rel="stylesheet" type="text/css" href="assets/css/styles.css">
    <link rel="icon" 
    type="image/ico" 
    href="assets/favicon.png">
  </head>
  <div>
    <div class="left-column">
  </div>';
  
  print($new_head);
  mast();
}

function mast() {
  $mast_out = '
    <a class="no-line-link" href="index.php">
      <div class="mast">
        <h1>W\'s</h1>
        <h2>Classy</h2>
        <h2>Personals</h2>
      </div>
    </a>
    <div class="central-column">';
  print($mast_out);
}

function possible_intents() {
  
  $form = '<option value="%s">%s</option>';
  $intentions = intentions_in_a_list();
  if (count($intentions)) {
    print('<div><label for="intent">Where are you posting?</label>
      <select id="intent" name="intent">
');  
    foreach ($intentions as $intent) {
      print sprintf($form, $intent, ucwords($intent));
    }
    print('</select></div>');
  }
}

function intentions_in_a_list() {
  return ['strictly platonic', 'women seeking women', 'women seeking men', 'men seeking women', 'men seeking men', 'misc romance', 'casual encounters', 'missed connections', 'rants and raves'];
}

function intents() {
  $intentions = intentions_in_a_list();
  $form = '<a class="no-line-link" href="listings.php?intent=%s">
      <div class="block %s">
        <p>%s</p>
      </div>';

  for ($i=0; $i < count($intentions); $i++) {
    $intent = slug($intentions[$i]);
    print sprintf($form, $intent, color_button($i), $intentions[$i]);  
  }
}

function listings($intent) {
  $form = '
  <a class="no-line-link" href="listing.php?id=%s&intent=%s">
    <div class="block %s">
      <p>%s</p>
    </div>
  </a>';
  
  //$titles = [['title' => 'I just want to be friends forever', 'id' => 1]];
  $DB = new Dao();
  $titles = $DB->getPosts($intent);
  
  if (count($titles) == 0) {
    print '<p class="error">It doesn\'t look like there are any postings here right now...</p>';
  }
  
  for ($i = 0; $i < count($titles); $i++) {
    $current = $titles[$i];
    $title = $current['title'];
    $id = $current['id'];
    print sprintf($form, $id, $intent, color_button($i), $title);
  }
}

function listing($id, $intent) {
  $DB = new Dao();
  $post = $DB->getPost($id);
  
  $header_form = '<h3><a class="no-line-link" href="listings.php?intent=%s">%s</a> > %s</h3>';
  $body_form = '<p class="body-text">%s</p>';
  $posting = $post['posting'];
  
  //description and title are all generated from a query
  $title = 'I just want to be friends forever!';
  print sprintf($header_form, $intent, ucfirst(unslug($intent)), $post['title']);
  print sprintf($body_form, $posting);
}

function color_button($current_index) {
  $colors = ['blue', 'yellow', 'gray'];
  $color_index = $current_index % count($colors);
  return $colors[$color_index];
}

function footer() {
  $footer = '
    <hr>

    <div id="footer">
        <li>
          <a class="no-line-link">© 2014 Zach Wegrzyniak</a>
        </li>
        <li>
          <a class="no-line-link" href="post.php">
            Post
          </a>
        </li>
        <li>
          <a class="no-line-link" href="logout.php">
            Log out
          </a>
        </li>
    </div>';
    print($footer);
}