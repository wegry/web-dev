<?php
ini_set('display_errors', 'On');

class Dao {

  private $host = "0.0.0.0";
  private $db = "c9";
  private $user = "wegry";
  private $pass = "";

  public function getConnection () {
    return
      new PDO("mysql:host={$this->host};dbname={$this->db}", $this->user,
          $this->pass);
  }

  public function savePost ($post_info) {
    $conn = $this->getConnection();
    $saveQuery =
        "INSERT into post
        (email, title, posting, date_posted, intent)
        VALUES
        (:email, :title, :posting, :date_posted, :intent)";
    
    $post_info['date_posted'] = date('Y-m-d');
    $q = $conn->prepare($saveQuery);
    
    $q->bindParam(":email", $post_info['email']);
    $q->bindParam(":title", $post_info['title']);
    $q->bindParam(":posting", $post_info['posting']);
    $q->bindParam(":date_posted", $post_info['date_posted']);
    $q->bindParam(":intent", $post_info['intent']);

    try {
      $q->execute();
    }
    catch (PDOException $exception) {
    // the query failed and debugging is enabled
      echo "<p>There was an error in query: $query</p>";
      echo $exception->getMessage();
      $pdoStatement = false;
    }
    echo var_dump($q);
  }
  
  public function getPosts ($intent) {
    $conn = $this->getConnection();
    $query = 'SELECT title, id FROM post 
    WHERE 
    intent = :intent
    and
    deleted = false
    ORDER BY 
    date_posted DESC';
    $q = $conn->prepare($query);
    $q->bindParam(':intent', $intent);
    
    $q->execute();
    $posts = $q->fetchAll();
    return $posts;
  }

  public function getPost($id) {
    $conn = $this->getConnection();
    $query = 'SELECT title, posting FROM post WHERE id = :id';
    $q = $conn->prepare($query);
    $q->bindParam(':id', $id);
    
    $q->execute();
    $post = $q->fetch();
    return $post;
  }

  public function createUser ($email, $password) {
    $conn = $this->getConnection();
    
    $existing = $this->check_for_existing_user($email);
    
    if (count($existing) > 0) {
      //Well, that didn't work out like I thought it would...
      return false;
    }
    else {
      $saveQuery =
        "INSERT INTO user
        (email, password)
        VALUES
        (:email, :password)";
      $q = $conn->prepare($saveQuery);
      $q->bindParam(":email", $email);
      $q->bindParam(":password", encrypt_password($password));
      $q->execute();
      return true;
    }
  }
  
  private function check_for_existing_user($email) {
    $conn = $this->getConnection();

    $check = 'SELECT * FROM user WHERE email = :email';
    $q = $conn->prepare($check);
    $q->bindParam(':email', $email);
    $q->execute();
    return $q->fetchAll();
  }
  
  public function check_password_for_user($email, $password) {
    $existing = $this->check_for_existing_user($email);
    if (count($existing) == 1) {
      $existing = $existing[0];
      $existing_password = $existing['password'];
      $hashed = encrypt_password($password);
      return $password == $existing_password;
    }
    return false;
  }
} // end Dao 

function encrypt_password($password) {
  return hash("sha256", $password . 'kitten mittens');
}