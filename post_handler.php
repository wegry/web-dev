<?php
// login_handler.php
ini_set('display_errors', 'On');
require_once('Dao.php');
require_once('DRY.php');

function check_valid_post($posting) {
    print('This is the beginning of check post.');
    print_r($posting);
    $errors = [];
    $presets = [];
    $presets['title'] = dictionary_get($posting, 'title');
    $presets['intent'] = dictionary_get($posting, 'intent');
    $presets['posting'] = trim(dictionary_get($posting, 'posting'));
    
    $title = trim(dictionary_get($posting, 'title'));
    if ($title == '') {
        $errors['title'] = 'A post title is required.' . $posting['title'];
    } 
    elseif (!preg_match('/W/', $title)) {
        $errors['title'] = dictionary_get($errors, 'title') . 'The title needs to be at least two letters too...';
    }
    if (!in_array($posting['intent'], intentions_in_a_list())) {
        $errors['intent'] = 'One of the selectable intents must be used.';
    }
    if (trim($posting['email']) == '') {
      header('Location:login.php');
    }
    if (trim(htmlspecialchars($posting['posting'])) == '') {
        $errors['posting'] = 'You should probably write something here.';
    }
    return [$presets, $errors];
}

session_start();
// For simplification Lets pretend I got these login credentials from an SQL table.

$DB = new Dao();
print '<p>Made it here</p>';

$_SESSION['status'] = [];
$_SESSION['presets'] = [];

//if ($DB->check_password_for_user($_POST["email"], $_POST["password"])) {
$_POST['posting'] = htmlspecialchars(dictionary_get($_POST, 'posting'));
$results = check_valid_post($_POST);
$errors = $results[1];
$presets = $results[0];

if (count($errors) == 0) {
    $_SESSION['status'] = [];
    $_SESSION['presets'] = [];
    $DB = new Dao();
    $DB->savePost($_POST);
    header("Location:post_success.php");
} else {
    print '<p>Post failed...</p>';
  $_SESSION['status'] = $errors;
  $_SESSION['presets'] = $presets;
  print_r($_SESSION);

  

  //header("Location:post.php");
}
?> 