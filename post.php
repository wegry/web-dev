<?php
  ini_set('display_errors', 'On');
  include 'DRY.php';
  session_start();

  if (!(isset($_SESSION["access_granted"]) && $_SESSION["access_granted"])) {
    header("Location:login.php");
  }
  
  $email = dictionary_get($_SESSION, "email_preset");
  
  $status = [];
  if (isset($_SESSION['status'])) {
    $status = $_SESSION['status'];
    unset($_SESSION['status']);
  } 

  $presets = [];
  if (isset($_SESSION['presets'])) {
    $presets = $_SESSION['presets'];
    unset($_SESSION['presets']);
  }
  
  
  head('Home');
?>
    <h3>What're you posting?</h3>
    <form action="post_handler.php" method="POST">
      <div>
      <?php
        if (isset($status['title'])) {
          print('<p class="error">' . $status['title'] . '</p>');
        }
        ?>
      <input autofocus="true" type="text" id="title" name="title" placeholder="Title" <?php if (dictionary_get($presets, 'title') != '') {
      print 'value="' . dictionary_get($presets, 'title') . '"';
      } ?> />
      </div>
      <?php 
        if (isset($status['intent'])) {
          print('<p class="error">' . $status['intent'] . '</p>');
        }
        possible_intents(); 
        if (isset($status['posting'])) {
          print('<p class="error">' . $status['posting'] . '</p>');
        }
      ?>
      <textarea id="posting" name="posting" placeholder="Write them something nice!">
        <?php 
        if (trim(dictionary_get($presets, 'posting')) != '') {
          print(trim(dictionary_get($presets, 'posting')));
      } ?>
      </textarea>
      <input type="hidden" placeholder="email" name="email" id="email" value="<?php echo $email; ?>"/>
      <div>
        <input class="submit rust" type="submit" id="post" name="submit" value="post">
      </div>
    </form>
    <?php
      footer();
    ?>
  </div>

</div>
</html>