<?php
ini_set('display_errors', 'On');

function slug($string) {
  $intent = explode(' ', $string);
  return join('_', $intent);
}

function unslug($string) {
  $intent = explode('_', $string);
  return join(' ', $intent);
}

function dictionary_get($dict, $key) {
  if (isset($dict[$key])) {
    return $dict[$key];
  }
  return '';
}