<?php
// login.php
  session_start();

  if (isset($_SESSION["access_granted"]) && $_SESSION["access_granted"]) {
    header("Location:post.php");
  }

  $email = "";
  if (isset($_SESSION["email_preset"])) {
    $email = $_SESSION["email_preset"];
  }
  require_once('DRY.php');
  head('Log in');
?>

    <h3>Log in</h3>
    <?php
    if (isset($_SESSION["status"])) {
      echo '<div id="status"><p class="error">' .  $_SESSION["status"] . "</p></div>";
      unset($_SESSION["status"]);
    }
    ?>
    <form action="login_handler.php" method="POST">
      <div>
        <input autofocus="true" type="text" placeholder="email" name="email" id="email" value="<?php echo $email; ?>"/>
      </div>
      <div>
        <input type="password" placeholder="password" name="password" id="password" value=""/>
      </div>
      <div>
        <input class="submit" type="submit" name="submit" id="login" value="Login"/>
      </div>
    </form>
    <a class="no-line-link" href="register.php">Register</a>
  </body>
</html>