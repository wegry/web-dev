<?php
ini_set('display_errors', 'On');
require_once('Dao.php');
require_once('utilities.php');

session_start();

$email = trim(dictionary_get($_POST, 'email'));
$password = trim(dictionary_get($_POST, 'password'));
$check = trim(dictionary_get($_POST, 'check'));
$errors = [];

if (trim($email) == '') {
  $errors['email'] = 'We\'re gonna need an email, buddy.';
}

if (trim($password) == '') {
  $errors['password'] = 'We\'re gonna need a password.';
} 
elseif ($password !== $check) {
  $errors['password'] = 'Passwords do not match';
}

if (count($errors) > 0) {
  header('Location:register.php');
}

$DB = new Dao();

if ( $DB->createUser($email, $password)) {
  $_SESSION['email_preset'] = $email;
  $_SESSION['access_granted'] = true;
  header("Location:post.php");
} 
else {
  $errors['email'] = 'Email already taken...';
  $_SESSION['errors'] = $errors;
  $_SESSION["access_granted"] = true;
  header("Location:register.php");
}