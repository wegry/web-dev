<?php
// login.php
    require_once('DRY.php');
  session_start();

  if (isset($_SESSION["access_granted"]) && $_SESSION["access_granted"]) {
    header("Location:post.php");
  }

  $email = dictionary_get($_SESSION, 'email_preset');
  $status = dictionary_get($_SESSION, 'status');
  head('Register');
?>

    <h3>New user registration</h3>
    <?php
    if ($status) {
      echo '<div id="status"><p class="error">' .  $_SESSION["status"] . "</p></div>";
      unset($_SESSION["status"]);
    }
    ?>
    <form action="registration_handler.php" method="POST">
      <div>
        <input autofocus="true" type="text" placeholder="email" name="email" id="email" value="<?php echo $email; ?>"/>
      </div>
      <div>
        <input type="password" placeholder="password" name="password" id="password" value=""/>
      </div>
      <div>
        <input type="password" placeholder="reenter your password" name="check" id="check" value=""/>
      </div>
      <div>
        <input class="submit" type="submit" name="submit" id="login" value="create"/>
      </div>
    </form>
    <a class="no-line-link" href="login.php">Log in</a>
  </body>
</html>